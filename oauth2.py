#!/usr/bin/env python

import argparse
import json
import sys
import urllib
import urlparse
import webbrowser

from textwrap import dedent
from threading import Event

import requests
from httptest import testserver

parser = argparse.ArgumentParser()
parser.add_argument('-c', '--client_id', help='the OAuth 2 client_id',
                    required=True)
parser.add_argument('-s', '--secret', help='the OAuth 2 client secret',
                    required=True)
parser.add_argument('--host', help="Bitbucket's URL",
                    default='https://bitbucket.org')
parser.add_argument('--apihost', help="Bitbucket's API URL",
                    default='https://api.bitbucket.org')
parser.add_argument('-p', '--callback_port', required=True, type=int,
                    help="the consumer's callback URL's port")
parser.add_argument('url', help='the API endpoint to access')
args = parser.parse_args()


auth_url = (urlparse.urljoin(args.host, '/site/oauth2/authorize') + '?' +
            urllib.urlencode({'client_id': args.client_id,
                              'response_type': 'code'}))

latch = Event()


def app(environ, start_response):
    latch.set()
    start_response('200 OK', [('Content-type', 'text/html')])
    return dedent("""<!DOCTYPE html>
        <html>
        <head>
            <script>
            window.open('','_self').close();
            </script>
        </head>
        <body>
            <h2>Success</h2>
        </body>
        </html>
        """)

with testserver(app, port=(args.callback_port,)) as server:
    print >> sys.stderr, 'Waiting for callback at', server.url(), '...'
    webbrowser.open(auth_url)
    latch.wait()

req = server.log()[0][0]
callback = urlparse.urlsplit(req.path)
params = urlparse.parse_qs(callback.query)
if 'error' in params:
    print >> sys.stderr, ('Authorization failed:', params['error'],
                          params.get('error_description'))
elif 'code' not in params:
    print >> sys.stderr, 'Unknown callback failure:', req.path
else:
    code = params.get('code')[0]
    resp = requests.post(
        urlparse.urljoin(args.host, '/site/oauth2/access_token'),
        data={'grant_type': 'authorization_code',
              'code': code},
        auth=(args.client_id, args.secret))
    resp.raise_for_status()
    values = resp.json()
    print >> sys.stderr, values

    resp = requests.get(args.url,
                        headers={'Authorization':
                                 'Bearer ' + values['access_token']})
    resp.raise_for_status()
    if resp.headers.get('Content-Type', '').startswith('application/json'):
        print json.dumps(resp.json(), indent=2)
    else:
        print resp.content
